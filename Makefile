.PHONY: help install

help : Makefile
	@sed -n 's/^##//p' $<

STATE-DIR := .make-state
EXTERNALS-DIR := ~/src/externals

STOW := stow --verbose

$(STATE-DIR):
	mkdir -p $@

$(EXTERNALS-DIR):
	mkdir -p $@


## install  : install everything
install: htop tmux fzf ntfy git-config


## htop     : a better top command
.PHONY: htop
htop: $(STATE-DIR)/htop

$(STATE-DIR)/htop: | $(STATE-DIR)
	sudo apt install htop
	@touch $@


## tmux     : a terminal multiplexer
.PHONY: tmux
tmux: $(STATE-DIR)/tmux ~/.tmux.conf

TMUX-YANK-DIR := $(EXTERNALS-DIR)/tmux-yank

$(STATE-DIR)/tmux: $(STATE-DIR)/tmuxp $(STATE-DIR)/tmux-yank | $(STATE-DIR)
	sudo apt install tmux
	@touch $@

$(STATE-DIR)/tmuxp: | $(STATE-DIR)
	sudo pip install --user tmuxp
	@touch $@

$(STATE-DIR)/tmux-yank: $(STATE-DIR)/xsel | $(STATE-DIR) $(EXTERNALS-DIR)
	git clone https://github.com/tmux-plugins/tmux-yank $(TMUX-YANK-DIR)
	@touch $@

$(STATE-DIR)/xsel: | $(STATE-DIR)
	sudo apt install xsel
	@touch $@

~/.tmux.conf:
	$(STOW) tmux

## fzf      : a fuzzy finder for bash
.PHONY: fzf
fzf: $(STATE-DIR)/fzf

FZF-DIR := $(EXTERNALS-DIR)/fzf

$(STATE-DIR)/fzf: $(FZF-DIR)/install | $(STATE-DIR)
	$(FZF-DIR)/install
	@touch $@

$(FZF-DIR)/install: $(EXTERNALS-DIR)
	git clone --depth 1 https://github.com/junegunn/fzf.git $(FZF-DIR) || (cd $(FZF-DIR) && git pull)


## ntfy     : a notification utility
.PHONY: ntfy
ntfy: $(STATE-DIR)/ntfy

$(STATE-DIR)/ntfy: | $(STATE-DIR)
	sudo pip install --user ntfy
	@touch $@


## git-config : install our configuration for git
.PHONY: git-config
git-config: ~/.gitconfig

~/.gitconfig:
	$(STOW) git
