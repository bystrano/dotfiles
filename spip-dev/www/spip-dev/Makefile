.PHONY: all		# build
.PHONY: clean # clean
.PHONY: up		# update
.PHONY: watch # watch and compile
.PHONY: live	# start a browser-sync session


# Configuration #

SASS    = ./node_modules/.bin/node-sass
WATCH		= ./node_modules/.bin/chokidar
IMGMIN  = ./node_modules/.bin/imagemin
BSYNC   = ./node_modules/.bin/browser-sync

GIT_SVN_DIR = plugins/git-svn/
GIT_DIR			= plugins/git/

LOGOS_ROLES_DIR		= plugins/git-svn/logos_roles/
SAISIE_LISTE_DIR	= plugins/git-svn/saisie_liste/


# Init #

ROOT_DIR = $(shell pwd)

WATCH_SRC  = $(LOGOS_ROLES_CSS_SRC)  $(SAISIE_LISTE_CSS_SRC)
WATCH_DIST = $(LOGOS_ROLES_CSS_DIST) $(SAISIE_LISTE_CSS_DIST)


# Main Rules #

all: logos_roles_build saisie_liste_build
clean: logos_roles_clean saisie_liste_clean
up: plugins_up


.PHONY: plugins_up
.ONESHELL:
plugins_up: init # hack to run the recipe every time
	@for plugin in $(wildcard $(GIT_SVN_DIR)*)
	@do
		@cd $$plugin ; echo "maj $$plugin" ; git svn rebase ; cd $(ROOT_DIR)
	@done
	@for plugin in $(wildcard $(GIT_DIR)*)
	@do
		@cd $$plugin ; echo "maj $$plugin" ; git pull --rebase ; cd $(ROOT_DIR)
	@done

# this is not PHONY
init:


watch:
	@$(WATCH) $(WATCH_SRC) --verbose --initial --command "$(MAKE) --no-print-directory"

live:
	@$(BSYNC) start --proxy 'localhost' --startPath '/spip-dev/' --files $(WATCH_DIST) --logLevel silent &
	@$(MAKE) watch --no-print-directory


# Logos Roles #

LOGOS_ROLES_CSS_SRC		= $(wildcard $(LOGOS_ROLES_DIR)css/*.scss)
LOGOS_ROLES_CSS_DIST	= $(patsubst %.scss, %.css, $(LOGOS_ROLES_CSS_SRC))

.PHONY: logos_roles_build
logos_roles_build: $(LOGOS_ROLES_CSS_DIST)
	@echo $(LOGOS_ROLES_CSS_DIST)

$(LOGOS_ROLES_CSS_DIST): $(LOGOS_ROLES_CSS_SRC)
	$(SASS) --source-map $@.map $< $@

.PHONY: logos_roles_clean
logos_roles_clean:
	-rm $(LOGOS_ROLES_CSS_DIST)


# Saisie Liste #

SAISIE_LISTE_CSS_SRC	= $(wildcard $(SAISIE_LISTE_DIR)*.scss)
SAISIE_LISTE_CSS_DIST = $(patsubst %.scss, %.css, $(SAISIE_LISTE_CSS_SRC))

.PHONY: saisie_liste_build
saisie_liste_build: $(SAISIE_LISTE_CSS_DIST)
	@echo $(SAISIE_LISTE_CSS_DIST)

$(SAISIE_LISTE_CSS_DIST): $(SAISIE_LISTE_CSS_SRC)
	$(SASS) --source-map $@.map $< $@

.PHONY: saisie_liste_clean
saisie_liste_clean:
	-rm $(SAISIE_LISTE_CSS_DIST)
