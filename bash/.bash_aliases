# update system packages
alias up="sudo apt update && sudo apt upgrade && sudo apt autoremove"

# fetch and index emails
alias ms="mbsync quick && mu index"

# shellcheck disable=SC2139
# editor
alias e="$EDITOR"
alias magit='e -nu -e \(magit-status\)'
alias mu4e='e -nu -e \(mu4e\)'

# améliorations : https://remysharp.com/2018/08/23/cli-improved
alias top=htop
alias more=less

# docker
alias dki="sudo docker image"
alias dkc="sudo docker container"
alias dks="sudo docker stack"

# pretty print logs
alias logpp="sed -e 's/\\\\n/\\n/g' -e 's/\\\\t/  /g'"
alias logfilterstatic="grep -vE '\\.(mp3|jpe?g|png|js|css|svg|otf|ico)'"

logtrim() {
    awk -v trim="$1" '{ print substr($0, 0, trim); }'
}

emacs-update-conf() {
    cd ~/.emacs.d || return 1;
    git pull --rebase || return 1;
    cd ..;
    emacs --eval '(progn (configuration-layer/update-packages t) (kill-emacs))' && e -c &
}

tmux-ssh-pudu() {
    tmux new -ds pi 'ssh -p 2220 pi@10.0.0.1'
}

# Usage : pdf-optimize $INPUT_FILE $OUTPUT_FILE
pdf-optimize() {

    gs -sDEVICE=pdfwrite -q -dPDFSETTINGS=/ebook -o "${2:-out.pdf}" "$1"
}
