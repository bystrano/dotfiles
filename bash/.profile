# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin directories
PATH="$HOME/bin:$HOME/.local/bin:$HOME/.cabal/bin:$HOME/.config/composer/vendor/bin:$PATH"

export GPGKEY=D3A02C26

# For the Renoise CDP tool
export CDP_SOUND_EXT=wav

export EDITOR="emacsclient -a=''"
export VISUAL=$EDITOR

# configure the ntfy tool : https://github.com/dschep/ntfy
export AUTO_NTFY_DONE_IGNORE="vi emacs nano less more emacsclient spotify shutdown reboot screen tmux man info ssh top"
eval "$(ntfy shell-integration)"

export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.node_modules/bin:$PATH"
